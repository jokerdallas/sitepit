const express = require('express');
const bodyParser = require('body-parser');
const server = express();
server.use(express.static("public"));
server.set('view engine', 'ejs');

const app = require('./routes');
server.use('/', app);
const api = require('./api');
server.use('/api', api);

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: false}));



const dbConfig = require('./config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const option = {
    connectTimeoutMS: 30000,
    keepAlive: 300000,
    useNewUrlParser: true,
};

mongoose.connect(dbConfig.url, option).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

mongoose.set('useCreateIndex', true);

server.listen(8000, () => {
    console.log("Example app listening at http://localhost:8000/")
});
