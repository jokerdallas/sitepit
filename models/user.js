var mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

var notificationSchema = new mongoose.Schema({
    Rid: {type: String, required: false, unique: false},
    date: {type: Date, required: true, unique: false},
    size: {type: Number, required: false, unique: false},
});

var boxSchema = new mongoose.Schema({
    adresse: {type: String, required: true, unique: false},
    pays: {type: String, required: true, unique: false},
    ville: {type: String, required: true, unique: false},
    code_postal: {type: String, required: true, unique: false},
    max_size: {type: Number, required: true, unique: false},
    size: {type: Number, required: true, unique: false},
});

var userSchema = new mongoose.Schema({
    email: {type: String, required: true, unique: true},
    psw: {type: String, required: true},
    nom: {type: String, required: true, unique: false},
    prenom: {type: String, required: true, unique: false},
    adresse: {type: String, required: true, unique: false},
    pays: {type: String, required: true, unique: false},
    ville: {type: String, required: true, unique: false},
    code_postal: {type: String, required: false, unique: false},
    phone: {type: String, required: true, unique: false},
    box: boxSchema,
    notifications: [notificationSchema],
    token: {type: String, required: false, unique: false}
});

userSchema.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.psw;
    return obj;
};

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);
