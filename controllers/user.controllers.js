const User = require('../models/user');


// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    User.find().then(users => {
            res.send(users);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
