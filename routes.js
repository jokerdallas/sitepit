var express = require('express');
var app = express.Router();
var bodyParser = require('body-parser');
app.use('/static', express.static('public'));
var User = require('./models/user');
var user = require('./controllers/user.controllers');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.render('../index.ejs')
});

app.get('/contact', function (req, res) {
    res.render('contact.ejs')
});

app.get('/login', function (req, res) {
    return res.render("login.ejs", {message: ''})
});

app.get('/produit', function (req, res) {
    res.render('produit.ejs')
});

app.get('/commander', function (req, res) {
    res.render('commander.ejs')
});


app.get('/users', user.findAll);
app.post('/addUser', addUser);


app.post('/signup', function (req, res) {
    console.log("adding user");

    addUser(req, res);
});

app.get('/updateUser', function (req, res) {
    console.log("updating user");
    updateUser(req, res);

});

app.post('/auth', function (req, res) {

    User.findOne({email: req.body.email}, function (err, user) {

        if (err) {
            console.log("printer error");
            return res.render("login.ejs", {message: 'Login error'})
        }

        if (!user) {
            console.log("printer no user");
            return res.render("login.ejs", {message: 'Utilisateur inconnu'})
        }

        if (req.body.psw !== user.psw) {
            console.log("printer psw wrong");
            return res.render("login.ejs", {message: 'Mot de passe incorrect'})
        }
        console.log("printer" + user);
        return res.render("edit-profile-commander.ejs", {user: user})
    });
});


function addUser(req, res) {

    if (!req.body) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    console.log("printer" + req.body.nom);
    console.log(req.body);

    User.findOne({email: req.body.email}, function (err, found) {

        if (err) {
            console.log("printer error");
            return res.render("login.ejs", {message: 'Login error'})
        }

        if (!found) {
            user = new User({
                email: req.body.email,
                psw: req.body.psw,
                nom: req.body.nom,
                prenom: req.body.prenom,
                adresse: req.body.adresse,
                pays: req.body.pays,
                ville: req.body.ville,
                code_postal: req.body.code_postal,
                phone: req.body.phone,
                box: {
                    adresse: req.body.adresse,
                    pays: req.body.pays,
                    ville: req.body.ville,
                    code_postal: req.body.code_postal,
                    max_size: "50",
                    size: "0"
                },
                notification: [],
                token: ""
            });

            user.notifications = [{Rid: "x", date: Date.now(), size: null},];

            user.save();
            res.render('login.ejs', {message: 'Vous venez de créer un nouveau compte'})
        }

        else {
            res.render('login.ejs', {message: 'Utilisateur déja éxistant'})
        }

        // Create a Note

    });
}

app.post('/updateUser', function (req, res) {


    console.log(req.body.old + " is the mvp");
    console.log(req.body.email + " is the email");

    User.findOne({email: req.body.old}, function (err, user) {

        if (err) {
            res.render('login.ejs', {message: 'Echec de modification'})
        }

        else {

            if (user != null) {
                user.nom = req.body.nom;
            user.prenom = req.body.prenom;
            user.email = req.body.email;
            user.adresse = req.body.adresse;
            user.ville = req.body.ville;
            user.pays = req.body.pays;
            user.code_postal = req.body.code_postal;
            user.phone = req.body.phone;

            if (req.body.psw !== "") {
                user.psw = req.body.psw;
            }

            user.save();

            res.render('login.ejs', {message: 'Modification réussie'})
        }
        else {
                res.render('login.ejs', {message: 'Echec de modification'})
            }
        }

    });

});

app.post('/updateBox', function (req, res) {


    User.findOne({email: req.body.old}, function (err, user) {

        if (err) {
            res.render('login.ejs', {message: 'Echec de modification'})
        }

        else {

            if (user != null) {
                user.box.adresse = req.body.adresse;
                user.box.ville = req.body.ville;
                user.box.pays = req.body.pays;
                user.box.code_postal = req.body.code_postal;


                user.save();

                res.render('login.ejs', {message: 'Modification réussie'})
            }
            else {
                res.render('login.ejs', {message: 'Echec de modification'})
            }
        }

    });

});



module.exports = app;
