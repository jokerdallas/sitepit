var express = require('express');

var api = express.Router();

const User = require('./models/user');

var FCM = require('fcm-node');

var fcm = new FCM('AAAA2kh-ZxM:APA91bF-mK12HNQJWGZ2CAqKUFeahYW8vUsKF09D16XkgjK1sUymcNiZFxS4eTdvNV7ukaXC99inPDE-cWrAYvanfGdSCO_1kh3uKFBS1NGzQX7hDLC7lQuuN--rprTC0LuftFERaIdA');

// curl -H "Accept:application/json" http://localhost:8000/users | python -m json.tool -> pour tester

api.post('/auth', function (req, res) {

    User.findOne({email: req.body.email}, function (err, user) {

        if (err) {
            console.log("printer error");
            return res.status(505).send('Server error');
        }

        if (!user) {
            console.log("printer no user");
            return res.status(500).send('User not found');
        }

        if (req.body.psw !== user.psw) {
            console.log("printer psw wrong");
            return res.status(500).send('Wrong password');
        }

        console.log("printer" + user);

        return res.status(200).json(user)
    });

});

api.post('/getUser', function (req, res) {

    User.findOne({email: req.body.email}, function (err, user) {

        if (err) {
            return res.status(500).json(user)
        }

        else {
            return res.status(200).json(user)
        }
    });

});

// curl POST -H "Content-Type:application/json" http://localhost:8000/api/sendNotification -d '{"Rid":"001","size":"15"}' | python -m json.tool

api.post('/sendNotification', function (req, res) {

    var tokens = [];

    console.log("Starting  notification with body : " + req.body.size);

    if (req.body.size != null) {


    User.find().then(users => {

        var send = false;

        for (let i = 0; i < users.length; i++) {

            console.log("there is " + users.length + " users");

            if (req.body.size > users[i].box.size || req.body.size === -1) {
                send = true;
                users[i].notifications.push({Rid: req.body.rid, date: Date.now(), size: req.body.size});
            }

            if (req.body.size >= 0) {
                users[i].box.size = req.body.size;
            }

            if (users[i].token !== "") {
                tokens.push(users[i].token);
                console.log("pushed" + tokens.length);
            }

            users[i].save();
        }

        console.log("number token " + tokens.length);

        if (send) {

            for (let i = 0; i < tokens.length; i++) {

                console.log("notification " + i);
                console.log("token numero " + i + ": " + tokens[i]);

                var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    to: tokens[i],

                    notification: {
                        title: 'Notification',
                        body: 'Nouveau courrier'
                    },
                };

                fcm.send(message, function (err, response) {
                    if (err) {
                        console.log("Something has gone wrong!");
                    } else {
                        console.log("Successfully sent with response: ", response);
                    }
                });
            }
        }

        return res.status(200).json("success")

    });
}

});

api.post('/updateUser', function (req, res) {


    User.findOne({email: req.body.user.email}, function (err, user) {

        if (err) {
            return res.status(500).json(user)
        }

        else {

            user.nom = req.body.user.nom;
            user.prenom = req.body.user.prenom;
            user.email = req.body.user.email;
            user.adresse = req.body.user.adresse;
            user.ville = req.body.user.ville;
            user.pays = req.body.user.pays;
            user.code_postal = req.body.user.code;
            user.phone = req.body.user.phone;
            user.box = req.body.user.box;
            user.notifications = req.body.user.notifications;

            user.save();

            return res.status(200).json(user)
        }

    });

});

api.post('/storeToken', function (req, res) {

    User.findOne({email: req.body.email}, function (err, user) {

        if (err) {
            return res.status(500).json(user)
        }

        else {

            user.token = req.body.token;

            user.save();

            return res.status(200).json(user)
        }
    });

});


module.exports = api;
